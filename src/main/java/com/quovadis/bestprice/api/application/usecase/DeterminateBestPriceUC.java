package com.quovadis.bestprice.api.application.usecase;

import com.quovadis.bestprice.api.application.service.facade.PriceFacade;
import com.quovadis.bestprice.api.rest.dto.PriceResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class DeterminateBestPriceUC {

    private final PriceFacade priceFacade;

    public Flux<String> determinate(String description) {

        return priceFacade.getPrice(description);
    }
}
