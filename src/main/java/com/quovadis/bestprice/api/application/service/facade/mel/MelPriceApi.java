package com.quovadis.bestprice.api.application.service.facade.mel;

import com.quovadis.bestprice.api.application.service.facade.PriceApiService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service("mel")
public class MelPriceApi implements PriceApiService {
    @Override
    public Flux<String> getPrices(String description) {
        return Flux.just("100.0", "200.0", "160.0");
    }
}
