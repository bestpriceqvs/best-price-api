package com.quovadis.bestprice.api.application.service.facade;

import reactor.core.publisher.Flux;

public interface PriceApiService {
    Flux<String> getPrices(String description);
}
