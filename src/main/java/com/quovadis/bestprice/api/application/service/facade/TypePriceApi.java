package com.quovadis.bestprice.api.application.service.facade;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypePriceApi {
    MELI("mel", 1, true),
    FRAV("fra", 2, true),
    MUS("mus", 3, false);

    private String api;
    private Integer priority;
    private Boolean isActive;
}
