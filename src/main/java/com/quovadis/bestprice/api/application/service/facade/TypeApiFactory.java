package com.quovadis.bestprice.api.application.service.facade;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class TypeApiFactory {

    private BeanFactory beanFactory;

    public TypeApiFactory(BeanFactory beanFactory){
        this.beanFactory = beanFactory;
    }

    public PriceApiService getServiceByType(@NotNull TypePriceApi typePriceApi){
        return beanFactory.getBean(typePriceApi.getApi(), PriceApiService.class);
    }
}
