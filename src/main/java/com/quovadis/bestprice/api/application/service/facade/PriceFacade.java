package com.quovadis.bestprice.api.application.service.facade;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@Slf4j
public class PriceFacade {

    private TypeApiFactory typeApiFactory;
    private List<PriceApiService> priceApisService;

    public PriceFacade(TypeApiFactory typeApiFactory){
        this.typeApiFactory = typeApiFactory;
        this.priceApisService = Arrays.stream(TypePriceApi.values())
                .filter(TypePriceApi::getIsActive)
                .map(typePriceApi ->  this.typeApiFactory.getServiceByType(typePriceApi))
                .collect(Collectors.toList());
    }

    public Flux<String> getPrice(String description) {

        var response = new AtomicReference<Flux<String>>(Flux.empty());

        priceApisService.forEach(priceApiService -> {
            response.accumulateAndGet(priceApiService.getPrices(description), ((stringFlux, stringFlux2) -> stringFlux.mergeWith(stringFlux2)));
        });

        return response.get();
    }

}
