package com.quovadis.bestprice.api.application.service.facade.fra;

import com.quovadis.bestprice.api.application.service.facade.PriceApiService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service("fra")
public class FraApiService implements PriceApiService {
    @Override
    public Flux<String> getPrices(String description) {
        return Flux.just("150.0", "300.0", "120.0");
    }
}
