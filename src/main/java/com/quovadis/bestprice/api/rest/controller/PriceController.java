package com.quovadis.bestprice.api.rest.controller;

import com.quovadis.bestprice.api.application.usecase.DeterminateBestPriceUC;
import com.quovadis.bestprice.api.rest.dto.PriceResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping(path = "api/v1/prices")
@Slf4j
@RequiredArgsConstructor
public class PriceController {

    private final DeterminateBestPriceUC determinateBestPriceUC;

    @Operation(summary = "Get emotions")
    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> getPrices(@RequestParam String description) {
        logger.info("Get Prices by {}", description);

        return determinateBestPriceUC.determinate(description);
    }
}
